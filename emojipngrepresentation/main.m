//
//  main.m
//  emojipngrepresentation
//
//  Created by jk9357 on 1/03/2015.
//  Copyright (c) 2015 World Wide Meth Distribution Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <getopt.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        if (argc==1) {
            printf("EmojiPNGRepresentation v1.0 by jk9357\nUsage: -o <Output Path> [-s <font size>] [-f <font>]\n");
            exit(EXIT_FAILURE);
        }
        
        int opt= 0;
        
        char *path = NULL;
        int size=160;
        char *font="AppleColorEmoji";
        
        static struct option long_options[] = {
            {"path",    required_argument, 0,  'o' },
            {"size",   optional_argument, 0,  's' },
            {"font",   optional_argument, 0,  'f' },
            {0,           0,                 0,  0   }
        };

        int long_index =0;
        while ((opt = getopt_long(argc,argv,"o:s:f:",long_options,&long_index )) != -1) {
            switch (opt) {
                case 'o' : path=(char *)malloc(sizeof(optarg));
                    strcpy(path, optarg);
                    break;
                case 'f' : font=optarg;
                    break;
                case 's' : size=atoi(optarg);
                    break;
                default:
                    printf("EmojiPNGRepresentation v1.0 by jk9357\nUsage: -o <Output Path> [-s <font size>] [-f <font>]\n");
                    exit(EXIT_FAILURE);
            }
        }

        NSFont *emoji=[NSFont fontWithName:[NSString stringWithUTF8String:font] size:size];

        NSCharacterSet *emojiCharacters=emoji.coveredCharacterSet;
        
        int writtenFilesCount=0;
        int newEmojiCount=0;
        int totalEmojiCount=0;

        for (int plane = 0; plane <= 16; plane++) {
            if ([emojiCharacters hasMemberInPlane:plane]) {
                UTF32Char c;
                for (c = plane << 16; c < (plane+1) << 16; c++) {
                    if ([emojiCharacters longCharacterIsMember:c]) {
                        UTF32Char c1 = OSSwapHostToLittleInt32(c); // To make it byte-order safe
                        NSString *s = [[NSString alloc] initWithBytes:&c1 length:4 encoding:NSUTF32LittleEndianStringEncoding];
                        
                        NSData * characterData = [s dataUsingEncoding:NSUnicodeStringEncoding];
                        unichar* ptr = (unichar*)characterData.bytes;

                        totalEmojiCount++;
                        NSTextField *textField=[[NSTextField alloc]initWithFrame:CGRectMake(0, 0, 160, 160)];
                        textField.font=emoji;
                        textField.stringValue=s;
                        [textField sizeToFit];
                        
                        NSImage *character=[[NSImage alloc] initWithData:[textField dataWithPDFInsideRect:[textField bounds]]];
                        [character lockFocus] ;
                        NSBitmapImageRep *imgRep = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect(0.0, 0.0, [character size].width, [character size].height)] ;
                        [character unlockFocus] ;
                        
                        NSData *data = [imgRep representationUsingType: NSPNGFileType properties: nil];
                        NSString *filename;
                        if (ptr[2]==0) {
                            filename=[NSString stringWithFormat:@"%s 0x%x.png",font,ptr[1]];
                        } else {
                            filename=[NSString stringWithFormat:@"%s 0x%x 0x%x.png",font,ptr[1],ptr[2]];
                        }
                        
                        [data writeToFile:[[NSString stringWithUTF8String:path]stringByAppendingPathComponent:filename] atomically:NO];
                        writtenFilesCount++;
                        
                        CGRect originalFrame=textField.frame;
                        
                        BOOL containsFourPoints;
                        if (ptr[2]==0) {
                            ptr[2]=0xd83c;
                            ptr[3]=0xdffb;
                            containsFourPoints=NO;
                        } else {
                            ptr[3]=0xd83c;
                            ptr[4]=0xdffb;
                            containsFourPoints=YES;
                        }
                        
                        NSString *newStrr = [NSString stringWithCharacters:ptr length:5];
                        
                        textField.stringValue=newStrr;
                        [textField sizeToFit];
                        
                        if (originalFrame.size.width==textField.frame.size.width&&originalFrame.size.height==textField.frame.size.height) {
                            newEmojiCount++;
                            for (int i=1; i<6; i++) {
                                ptr[4]=0xdffa+i;
                                if (containsFourPoints) {
                                    filename=[NSString stringWithFormat:@"%s 0x%x 0x%x 0x%x 0x%x.png",font,ptr[1],ptr[2],ptr[3],ptr[4]];
                                } else {
                                    filename=[NSString stringWithFormat:@"%s 0x%x 0x%x 0x%x.png",font,ptr[1],ptr[2],ptr[3]];
                                }
                                NSString *characterString = [NSString stringWithCharacters:ptr length:5];
                                
                                NSTextField *textField=[[NSTextField alloc]initWithFrame:CGRectMake(0, 0, 160, 160)];
                                textField.font=emoji;
                                textField.stringValue=characterString;
                                [textField sizeToFit];
                                
                                NSImage *character=[[NSImage alloc] initWithData:[textField dataWithPDFInsideRect:[textField bounds]]];
                                [character lockFocus] ;
                                NSBitmapImageRep *imgRep = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect(0.0, 0.0, [character size].width, [character size].height)] ;
                                [character unlockFocus] ;
                                
                                NSData *data = [imgRep representationUsingType: NSPNGFileType properties: nil];
                                [data writeToFile:[[NSString stringWithUTF8String:path]stringByAppendingPathComponent:filename] atomically:NO];
                                writtenFilesCount++;
                            }
                        }
                    }
                }
            }
        }
        printf("Done. Wrote %d files.\n",writtenFilesCount);
        printf("Found %d characters (%d with variations).\n",totalEmojiCount,newEmojiCount);
    }
    return 0;
}